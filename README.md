# User Manual (Module for Drupal 9+)

Allows administrators to create a user manual for the site.

The purpose of this project is to add a place within the site for a manual
that is useful to site editors. This manual can be added/edited by
developers or site administrators. The hope is that this will help keep
manual more up to date, as well as make it more accessible to site
editorial teams.

To have dropdown user manual menu by topics in admin toolbar
you can enable admin_toolbar contrib module as dependency.

Functionality
------------------

- A new entity type (`user_manual`) is added. Any entities created will
appear at `/admin/user-manual`.
